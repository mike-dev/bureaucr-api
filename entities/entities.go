package entities

// Node ...
type Node struct {
	UUID     string  `json:"uuid"`
	Name     string  `json:"name"`
	Level    int64   `json:"level"`
	Children []*Node `json:"children"`
}

// User ...
type User struct {
	UUID  string
	Name  string
	Level int64
}
