package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	apiservice "bitbucket.org/mike-dev/bureaucr-api"
	"bitbucket.org/mike-dev/bureaucr-api/application"
	db "bitbucket.org/mike-dev/bureaucr-api/application/repository/db"
	"bitbucket.org/mike-dev/bureaucr-api/config"
	"bitbucket.org/mike-dev/bureaucr-api/go-sdk/helpers"
	"bitbucket.org/mike-dev/bureaucr-api/go-sdk/types"
	"bitbucket.org/mike-dev/bureaucr-api/users"
	userinfrastruct "bitbucket.org/mike-dev/bureaucr-api/users/infrastructure"
)

var (
	httpRouter *mux.Router
)

func main() {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	fs := flag.NewFlagSet("transvc", flag.ExitOnError)

	configFile := fs.String("config", "./config/config.json", "config file path")

	var settings config.Settings
	if types.StringValue(configFile) != "" {
		helpers.LoadSettingsFromFile(types.StringValue(configFile), &settings)
	} else {
		helpers.LoadSettingsFromEnvironmentVariable(&settings)
	}

	var logger log.Logger
	{
		logger = log.NewJSONLogger(os.Stdout)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	{
		httpRouter = mux.NewRouter()
		// DB can be added later
		storage := db.NewRepository(ctx)
		app := application.NewUserService(storage)
		svc := users.NewUsersService(app)
		endpoints := users.NewUsersEndpoints(svc)

		userinfrastruct.AddHTTPHandlers(httpRouter, endpoints)

		fmt.Println("starting", settings.HTTPPort)
		svr := http.Server{
			ReadTimeout:  apiservice.HTTPServerReadTimeout,
			WriteTimeout: apiservice.HTTPServerWriteTimeout,
			Addr:         net.JoinHostPort("", settings.HTTPPort),
			Handler:      httpRouter,
		}

		logger.Log("http", "starting", settings.HTTPPort)

		go func() {
			if err := svr.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				logger.Log(err)
				return
			}
		}()

		logger.Log("received system interruption", "signal", <-c)

		// stop the service
		if err := svr.Shutdown(context.Background()); err != nil && err != http.ErrServerClosed {
			logger.Log(err, "server shutdown")
			return
		}

		logger.Log("stop transaction service")
	}
}
