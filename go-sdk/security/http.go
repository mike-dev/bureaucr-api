package security

import (
	"context"
	httptransport "github.com/go-kit/kit/transport/http"
	"net/http"
)

// DecodeAuthorizationHeader ...
func DecodeAuthorizationHeader(next httptransport.DecodeRequestFunc) httptransport.DecodeRequestFunc {
	return func(ctx context.Context, request *http.Request) (req interface{}, err error) {
		if req, err = next(ctx, request); err != nil {
			return
		}
		return req, nil
	}
}
