package types

// StringValue ...
func StringValue(v *string) string {
	if v != nil {
		return *v
	}
	return ""
}
