package application

import (
	"bitbucket.org/mike-dev/bureaucr-api/entities"
	"bitbucket.org/mike-dev/bureaucr-api/users"
	"context"
)

// usersService ...
type usersService struct {
	storage UsersStorage
}

// UsersStorage ...
type UsersStorage interface {
	Manager(ctx context.Context, user1ID, user2ID string) (entities.User, error)
}

// NewUserService ...
func NewUserService(storage UsersStorage) users.Service {
	return &usersService{
		storage: storage,
	}
}

// GetManager ...
func (u usersService) GetManager(ctx context.Context, user1ID, user2ID string) (entities.User, error) {
	return u.storage.Manager(ctx, user1ID, user2ID)
}
