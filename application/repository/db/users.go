package db

import (
	"context"
	"encoding/json"

	"bitbucket.org/mike-dev/bureaucr-api/entities"
)

// Repo ..
type Repo struct {
}

// NewRepository ...
func NewRepository(ctx context.Context) (r *Repo) {
	r = &Repo{}
	return
}

func (r Repo) Manager(ctx context.Context, user1ID, user2ID string) (entities.User, error) {
	root := entities.Node{}
	err := json.Unmarshal(userData, &root)
	if err != nil {
		return entities.User{}, err
	}

	manager := findCommonManager(&root, user1ID, user2ID)
	u := entities.User{
		UUID:  manager.UUID,
		Name:  manager.Name,
		Level: manager.Level,
	}
	return u, nil
}

func findCommonManager(root *entities.Node, user1ID, user2ID string) entities.Node {
	nn := *findManager(root, user1ID)
	nn2 := *findManager(root, user2ID)
	bu := nn
	var commonManagers []entities.Node
	for nn.Level != 0 {
		for nn2.Level != 0 {
			if nn.UUID == nn2.UUID {
				commonManagers = append(commonManagers, nn)
				break
			}
			nn2 = *findManager(root, nn2.UUID)
		}
		nn = bu
		nn2 = *findManager(root, user2ID)
	}
	if len(commonManagers) == 0 {
		return entities.Node{}
	}

	maxLevelManager := commonManagers[0]
	for i := 1; i < len(commonManagers); i++ {
		if commonManagers[i].Level > maxLevelManager.Level {
			maxLevelManager = commonManagers[i]
		}
	}
	return maxLevelManager
}

func findManager(root *entities.Node, id string) *entities.Node {
	queue := make([]*entities.Node, 0)
	queue = append(queue, root)
	for len(queue) > 0 {
		nextUp := queue[0]
		queue = queue[1:]
		if len(nextUp.Children) > 0 {
			for _, child := range nextUp.Children {
				if child.UUID == id {
					return nextUp
				}
				queue = append(queue, child)
			}
		} else if nextUp.Level == 0 {
			return nextUp
		}
	}
	return nil
}

var userData = []byte(`
{
   "uuid":"123123-5475467-678678-67678",
   "name":"Claire",
   "level":0,
   "children":[
      {
         "uuid":"123123-5475467-678678-111",
         "name":"Bill",
         "level":1,
         "children":[
            {
               "uuid":"123123-5475467-678678-2222",
               "name":"Inga",
               "level":2,
               "children":[                  
               ]
            },
            {
               "uuid":"123123-5475467-678678-2221",
               "name":"Vasili",
               "level":2,
               "children":[
                  {
                     "uuid":"123123-5475467-678678-3331",
                     "name":"Angela",
                     "level":3,
                     "children":[
                        {
                           "uuid":"123123-5475467-678678-4441",
                           "name":"Anna",
                           "level":4,
                           "children":[                              
                           ]
                        }
                     ]
                  },
                  {
                     "uuid":"123123-5475467-678678-3333",
                     "name":"Mike",
                     "level":3,
                     "children":[                        
                     ]
                  },
                  {
                     "uuid":"123123-5475467-678678-3332",
                     "name":"Bob",
                     "level":3,
                     "children":[                        
                     ]
                  }
               ]
            }
         ]
      },
      {
         "uuid":"123123-5475467-678678-333",
         "name":"Mike",
         "level":1,
         "children":[
            {
               "uuid":"123123-5475467-678678-444",
               "name":"Marc",
               "level":2,
               "children":[                  
               ]
            },
            {
               "uuid":"123123-5475467-678678-555",
               "name":"Anna",
               "level":2,
               "children":[                  
               ]
            }
         ]
      },
      {
         "uuid":"123123-5475467-678678-666",
         "name":"Bob",
         "level":1,
         "children":[
            {
               "uuid":"123123-5475467-678678-777",
               "name":"Valery",
               "level":2,
               "children":[
                  
               ]
            }
         ]
      }
   ]
}
`)
