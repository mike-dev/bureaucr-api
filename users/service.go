package users

import (
	"bitbucket.org/mike-dev/bureaucr-api/entities"
	"context"
)

// Service is the domain interface for the users service
type Service interface {
	GetManager(ctx context.Context, user1ID, user2ID string) (entities.User, error)
}

// NewUsersService ...
func NewUsersService(s Service) Service {
	return &service{
		userService: s,
	}
}

type service struct {
	userService Service
}

// GetManager ...
func (s *service) GetManager(ctx context.Context, user1ID, user2ID string) (entities.User, error) {

	return s.userService.GetManager(ctx, user1ID, user2ID)
}
