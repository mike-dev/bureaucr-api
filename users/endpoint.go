package users

import (
	"bitbucket.org/mike-dev/bureaucr-api/entities"
	"context"
	"github.com/go-kit/kit/endpoint"
)

const (
	// GetUserManagerEndpoint ...
	GetUserManagerEndpoint = "getManager"
)

// Endpoints contains the endpoints for the service.
type Endpoints map[string]endpoint.Endpoint

// NewUsersEndpoints ...
func NewUsersEndpoints(upc Service) Endpoints {
	return Endpoints{
		GetUserManagerEndpoint: makeGetUserManagerEndpoint(upc),
	}
}

// NewGetUsersManagerDataRequest ...
func NewGetUsersManagerDataRequest() GetUsersManagerRequest {
	return GetUsersManagerRequest{}
}

// GetUserManagerRequest ...
type GetUsersManagerRequest struct {
	User1ID string
	User2ID string
}

// GetUsersManagerResponse ...
type GetUsersManagerResponse struct {
	User entities.User
}

func makeGetUserManagerEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req, ok := request.(GetUsersManagerRequest)
		if !ok {
			panic("type assertion GetUserManagerRequest failed")
		}
		u, err := s.GetManager(ctx, req.User1ID, req.User2ID)
		if err != nil {
			return nil, err
		}
		return GetUsersManagerResponse{u}, nil
	}
}
