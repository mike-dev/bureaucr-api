package infrastructure

import (
	api "bitbucket.org/mike-dev/bureaucr-api"
	"bitbucket.org/mike-dev/bureaucr-api/go-sdk/security"
	"bitbucket.org/mike-dev/bureaucr-api/users"
	"context"
	"encoding/json"
	"fmt"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"net/http"
)

// AddHTTPHandlers ...
func AddHTTPHandlers(r *mux.Router, endpoints users.Endpoints) {
	s := r.PathPrefix(fmt.Sprintf("/v1/bureaucr/{%s}/{%s}", api.User1IDField, api.User2IDField)).Subrouter()

	options := HTTPServerOptions()

	s.Methods(http.MethodGet).Path("/getUsersManager").Handler(httptransport.NewServer(
		endpoints[users.GetUserManagerEndpoint],
		security.DecodeAuthorizationHeader(decodeHTTPGetManagerRequest),
		encodeHTTPGetUsersManagerResponse,
		options...,
	))
}

func encodeHTTPGetUsersManagerResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(users.GetUsersManagerResponse)
	if !ok {
		return fmt.Errorf("type conversion error in  users response")
	}

	select {
	case <-ctx.Done():
		return errors.Wrap(fmt.Errorf("HTTP transport closed, response: %s", ctx.Err()), "")
	default:
		return encodeResponse(resp.User, w, http.StatusOK)
	}
}

func decodeHTTPGetManagerRequest(_ context.Context, req *http.Request) (interface{}, error) {
	if req.Body != nil {
		if err := req.Body.Close(); err != nil {
			return nil, fmt.Errorf("body cannot be closed: " + err.Error())
		}
	}
	request := users.NewGetUsersManagerDataRequest()
	vars := mux.Vars(req)
	var ok bool
	request.User1ID, ok = vars[api.User1IDField]
	if !ok {
		return nil, errors.New("cannot obtain User1ID from the http request")
	}
	request.User2ID, ok = vars[api.User2IDField]
	if !ok {
		return nil, errors.New("cannot obtain User2ID from the http request")
	}

	return request, nil
}

func encodeResponse(payload interface{}, w http.ResponseWriter, status int) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	data, err := json.Marshal(&payload)
	if err != nil {
		return errors.Wrap(err, "encode users response")
	}
	if _, err := w.Write(data); err != nil {
		return errors.Wrap(err, "users writer write")
	}
	return nil
}

func HTTPServerOptions() []httptransport.ServerOption {
	options := []httptransport.ServerOption{
		httptransport.ServerBefore(),
		httptransport.ServerAfter(),
	}
	return options
}
