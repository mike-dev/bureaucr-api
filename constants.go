package userservice

import "time"

const (
	// HTTPServerReadTimeout server read timeout
	HTTPServerReadTimeout = time.Second * 60
	// HTTPServerWriteTimeout server write timeout
	HTTPServerWriteTimeout = time.Second * 60
)
const (
	User1IDField = "user1Id"
	User2IDField = "user2Id"
)
