package config

// Settings ...
type Settings struct {
	HTTPTimeout string `json:"HTTP_TIMEOUT" env:"HTTP_TIMEOUT" default:"60"`
	HTTPPort    string `json:"HTTP_PORT" env:"HTTP_PORT"`
}
