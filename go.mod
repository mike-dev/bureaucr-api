module bitbucket.org/mike-dev/bureaucr-api

go 1.15

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.8.1
)